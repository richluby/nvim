function gray-start -d "Starts graylog"
  set current $PWD
  set dir $HOME/dev/graylog-docker-compose/open-core/
  cd $dir
  docker compose -f docker-compose.yml up -d --no-log-prefix
  cd $current
end
