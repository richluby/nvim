function dc-restart -d "Restart docker compose in the current directory."
  docker compose down
  docker compose up $argv
end
