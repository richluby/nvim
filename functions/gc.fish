function gc -d "Creates a template for use in committing."
  git add --interactive; or return $status

  set issue_project issues

  set branch (git rev-parse --abbrev-ref HEAD)
  set issue (printf "%s" $branch | rg -o "\d+" |head -n 1)
  set group_name (printf "%s" $branch | grep -Po "(?<=$org_name/).*(?=/)" || printf "")

  if test $issue
    set prefix "$org_name/$group_name/$issue_project#$issue"
  end

  set tmp_file (mktemp -p /tmp/ git-commit-XXX)

  printf "feat: " $prefix > $tmp_file

  git commit --file $tmp_file --trailer "refs=$prefix" --edit
  if test $status -eq 0
    rm $tmp_file
  end
  return $status
end
