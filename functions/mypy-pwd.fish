function mypy-pwd -d "Run mypy against the project in PWD"
  set options --no-error-summary --exclude ".*test.*" --ignore-missing-imports --warn-return-any --warn-unused-ignores --install-types
  if test -z $argv[1]
    mypy $options $PWD
  else
    mypy $options $argv
  end
end
