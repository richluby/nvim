function gchpd -d "Checkout main, pull main, delete current branch."
  set current_branch (git rev-parse --abbrev-ref HEAD)

  gchm; or return $status

  gplm; or return $status

  git branch -d $current_branch
end
