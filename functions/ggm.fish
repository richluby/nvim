function ggm -d "Git Get Main branch (into main_branch)"
  set main_branch (git rev-parse --abbrev-ref master 2>/dev/null)
  if test $status -ne 0
    set main_branch (git rev-parse --abbrev-ref main 2>/dev/null)
  end
  printf "%s" $main_branch
end
