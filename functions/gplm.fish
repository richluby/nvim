function gplm -d "Git Pull the default branch of the repo"
  set main_branch (ggm)

  set current_branch (git rev-parse --abbrev-ref HEAD)

  if test $current_branch = $main_branch
    printf "Pulling: %s\n" "$main_branch"
    git pull
  else
    printf "Fetching: %s\n" "$main_branch"
    git fetch origin $main_branch:$main_branch
  end
end
