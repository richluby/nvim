function gpla -d "Pull all git repos from a group"
  set api "/api/v4"
  set group_url "https://$GITLAB_DOMAIN$api/groups/$GITLAB_GROUP"

  set url_all_projects "$group_url/projects?include_subgroups=true&per_page=100"

  set PROJECT_PATH /tmp/gitlab_projects-$GITLAB_GROUP.json
  if test ! -e $PROJECT_PATH
    printf "Pulling new project list.\n"
    curl -sSL -k -o $PROJECT_PATH --header "PRIVATE-TOKEN: $GITLAB_TOKEN" $url_all_projects; or return $status
  end
  set project_urls (jq -r '.[].ssh_url_to_repo' <$PROJECT_PATH)
  set project_names (jq -r '.[].path_with_namespace' <$PROJECT_PATH)
  printf "URLS: %s\n" $project_urls

  for i in (seq 1 (count $project_names))
    set name (basename $project_names[$i])
    set project_dir $DEV_DIR/$name
    if test ! -e $project_dir
      printf "Pull: %s\n" $project_dir
      git clone $project_urls[$i] $project_dir
    else
      printf "Exis: %s\n" $project_dir
    end

    pushd $project_dir
      gplm
    popd
  end
end
