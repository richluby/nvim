function tcard -d "Helper for time cards" -a commit
  set current_directory $PWD
  set tcard_directory $HOME/dev/python/
  nvim $tcard_directory/time.md; or return $status

  if test $commit
    cd $tcard_directory
    git add time.md
    git commit -m "update time card"; or return $status
    git push
    cd $current_directory
  end
end

