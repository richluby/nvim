function file-size -d "Gets the size of the file" -a file
  set file_path $argv[1]
  printf "%d" (stat $file_path | head -n2 | tail -n1 | awk '{print $2}')
end

function yaml-diff-cluster -d "Diffs the deployed release with the current directory chart" 
  argparse --name=yaml-diff-cluster "release=" "namespace=" "resource=" "chart=?" -- $argv or return
  if test $status -ne 0
    printf "Failed to parse args.\n"
    return 1
  end

  if ! set -q _flag_release || ! set -q _flag_resource || ! set -q _flag_namespace
    printf "Failed to set a var.\n"
    printf "$_flag_namespace $_flag_resource $_flag_release\n"
    return 1
  end

  set release $_flag_release
  set namespace $_flag_namespace
  set resource $_flag_resource

  if test -z "$_flag_chart"
    printf "Set chart to default: [./chart/]\n"
    set chart ./chart/
  else
    set chart $_flag_chart
  end

  set deployed_file /tmp/cluster.yaml
  set new_file /tmp/incoming.yaml

  printf "Diff ns: [%s] c: [%s] rel: [%s] rsc: [%s]\n" $namespace $chart "$release" $resource

  set deployed (helm get manifest -n $namespace $release)
  if test $status -ne 0
    printf "Error getting manifest.\n"
    return 1
  end
  printf "%s" $deployed | yq eval ". |select(.metadata.name | test(\"$resource\"))" > $deployed_file

  set size (file-size $deployed_file)
  if test "$size" -eq 0 -o $status -ne 0
    printf "No cluster deployment found.\n"
    return 1
  end

  set proposed (helm template -n $namespace --debug $chart --values "$chart/values.yaml")
  if test $status -ne 0
    printf "Error building manifest.\n"
    return 1
  end
  printf "%s" proposed | yq eval ". |select(.metadata.name | test(\"$resource\"))" >$new_file

  set size (file-size $new_file)
  if test "$size" -eq -o or test $status -ne 0
    printf "No cluster deployment found.\n"
    return 1
  end

  dyff between $deployed_file $new_file

end
