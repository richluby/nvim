function yaml-chart-resource -d "Displays the name resource from the current directory chart" -a resource
  set resource $argv
  helm template --debug chart/ --values chart/values.yaml | yq eval ". |select(.metadata.name | test(\"$resource\"))"
end
