function fish_greeting
end

function fish_prompt -d "Sets the fish prompt"
    set -l last_pipestatus $pipestatus
    set -lx __fish_last_status $status # Export for __fish_print_pipestatus.
    set -l normal (set_color normal)
    set -q fish_color_status
    or set -g fish_color_status --background=red white

    # Color the prompt differently when we're root
    set -l color_cwd $fish_color_cwd
    set -l suffix '>'
    if functions -q fish_is_root_user; and fish_is_root_user
        if set -q fish_color_cwd_root
            set color_cwd $fish_color_cwd_root
        end
        set suffix '#'
    end

    # Write pipestatus
    # If the status was carried over (e.g. after `set`), don't bold it.
    set -l bold_flag --bold
    set -q __fish_prompt_status_generation; or set -g __fish_prompt_status_generation $status_generation
    if test $__fish_prompt_status_generation = $status_generation
        set bold_flag
    end
    set __fish_prompt_status_generation $status_generation
    set -l status_color (set_color $fish_color_status)
    set -l statusb_color (set_color $bold_flag $fish_color_status)
    set -l prompt_status (__fish_print_pipestatus "[" "]" "|" "$status_color" "$statusb_color" $last_pipestatus)


    # Set  date
    set -l datestamp (date +"%H:%M:%S")

    echo -n -s $datestamp-(prompt_login)':' (set_color $color_cwd) "$PWD" $normal (fish_vcs_prompt) $normal " "$prompt_status $suffix \n " "
end

if status is-interactive
    set -gx EDITOR (command -v nvim)
    set -gx VISUAL $EDITOR
    function set_python_venv --on-event fish_prompt -d "Activate the python venv"
        if test -z $VIRTUAL_ENV
            if test -n "$FISH_VENV_IS_ACTIVE"
                set is_deactivated (deactivate >/dev/null)
                set -g FISH_VENV_IS_ACTIVE
            end
        else
            if test -z "$FISH_VENV_IS_ACTIVE"
                source $VIRTUAL_ENV/bin/activate.fish
                set -g FISH_VENV_IS_ACTIVE 1
            end
        end
    end

    set paths $HOME/.krew/bin
    if test (printf $PATH | rg ".krew/bin"; printf $status) -ne 0
        set PATH $PATH $paths
        set path_set 1
    end

    # Commands to run in interactive sessions can go here
    abbr dcls 'docker container ls --format "table {{.ID}} {{.Status}}\t {{.Names}}\t {{.Ports}}"'
    abbr ga 	'git add --interactive'
    abbr gb 	'git branch'
    abbr gch 	'git checkout'
    abbr gd 	'git diff'
    abbr gitlog 	'git log --oneline --graph'
    abbr gpl 	'git pull'
    abbr gps 	'git push'
    abbr gpsf 	'git push --force-with-lease'
    abbr gpss 	'git push -o ci.skip'
    abbr gr 	'git rebase --interactive'
    abbr gs 	'git status'
    abbr less 	'less -SR'
    abbr nv 	nvim
    abbr rgc    'rg -n --color always'
    abbr mx     'mise exec --'
    abbr mxc    'mise exec -- code .'
    abbr mr     'mise run'

    if test -f ~/.config/fish/org_vars.fish
        source ~/.config/fish/org_vars.fish
    end
    ~/.local/bin/mise activate --shims fish | source

    if test -f $HOME/dev
        cd $HOME/dev
    end
end
